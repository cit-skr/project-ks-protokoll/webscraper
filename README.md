# WebScraper

Scrapes documents from web, adds entry to database and dumps them to a file storage.


### Tasks:
- Connect with GraphQl Hasura server instead of postgres SQl server :done
- Create client for GraphQL and find a way to add schema through it. :done
- Fix bugs in the selenium webscraper :almost done
- Filter the pdfs in a better way
- add pause functionality
- Spider for scraping webpages with pagination
- Setup user agents (scrapy user agents)
- Setup ProxyMesh for IP masking ( scrapy proxy tool)


