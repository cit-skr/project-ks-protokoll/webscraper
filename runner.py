import os, sys, math, subprocess, argparse, traceback
from urllib.parse import urlparse
from subprocess import STDOUT, check_output

from ks_protokoll.spiders.utilities import mkdir_p
parent_folder_path = os.path.dirname( os.path.abspath(__file__))


from gql import gql, Client, AIOHTTPTransport, transport

# Select your transport with a defined url endpoint
transport = AIOHTTPTransport(
    url="http://172.25.113.217:8080/v1/graphql",
    headers={'Content-Type': 'application/json', 'x-hasura-admin-secret': 'cloud_runner'}
)

# Create a GraphQL client using the defined transport
client = Client(transport=transport, fetch_schema_from_transport=True)

def parse_db():
    urls_to_scrape = []
    query = """
    query MyQuery {
        ks_protokoll_schema_municipality_info(order_by: {municipality_id: asc}) {
            municipality_id
            url
            status
            http_status_code
            n_pdfs
        }
    }
    """
    urls_to_scrape = client.execute(gql(query)).get('ks_protokoll_schema_municipality_info')

    return urls_to_scrape


# spider_name = "pdf_crawler"
# start_url="https://medborgare.ekero.se/committees/kommunstyrelsen"
# domain_id=109


def run_spider(spider_name, start_url, domain_id, is_test = False, timeout_min = 10):
    print(start_url)
    error = False
    output = ""
    try:
        output = subprocess.call(
            ['scrapy', 'crawl', spider_name, '-a', f'start_url={start_url}', '-a', 
            f'domain_id={domain_id}', '-a', f'test={int(is_test)}'],
            stderr=STDOUT, timeout= timeout_min*60
        )
    except subprocess.TimeoutExpired as e:
        error = True
        output += str(e)
        traceback.print_exc()
    except BaseException as e:
        error = True
        output += str(e)
        print(output)
        traceback.print_exc()
    
    return error, output

def run_pdf_spider(is_test=False):
    query = """
        mutation {
            update_ks_protokoll_schema_municipality_info_by_pk(_set: {status: "%s"}, pk_columns: {municipality_id: %d}) {
                latest_update
                status
            }
        }
    """
    spider_name = "pdf_crawler"
    start_urls = parse_db()
    mkdir_p('output')
    urls_to_scrape = parse_db()
    for item in urls_to_scrape:
        try:
            if (item['http_status_code'] is None) or  (item['n_pdfs'] < 10) :
                municipality_id = int(item.get('municipality_id'))
                error, output = run_spider(spider_name,str(item['url']), municipality_id,is_test=is_test)
                # if error:
                #     client.execute(gql(query % (output, municipality_id)))

        except BaseException as e:
            print("########################## --------------")
            print(e)
            print("########################## --------------")

def run_diarium_spider(is_test=False):
    spider_name = "diarium_spider"
    mkdir_p('output')
    urls_to_scrape = parse_db()
    query = """
        mutation {
            update_ks_protokoll_schema_municipality_info_by_pk(_set: {status: "%s"}, pk_columns: {municipality_id: %d}) {
                latest_update
                status
            }
        }
    """
    for item in urls_to_scrape:
        try:
            if (item['http_status_code'] is None) or  (item['n_pdfs'] < 10):
                municipality_id = int(item.get('municipality_id'))
                error, output = run_spider(spider_name,str(item.get('url')),int(item.get('municipality_id')),is_test=is_test)
                if error:
                    client.execute(gql(query % (output, municipality_id)))
        except BaseException as e:
            print("########################## --------------")
            print(e)
            print("########################## --------------")
    
def test_diarium_spider(url):
    spider_name = "diarium_spider"
    mkdir_p('output')
    run_spider(spider_name,str(url),0,True)

"""
http://diariet.nassjo.se/documents
https://medborgare.ekero.se/committees/kommunstyrelsen
"""

# test_diarium_spider(url= "https://medborgare.ekero.se/committees/kommunstyrelsen")


def main(args):
    if args.scrapy:
        print("running scrapy spider")
        run_pdf_spider(is_test=args.test)
    elif args.selenium:
        print("running selenium spider")
        run_diarium_spider(is_test=args.test)
    else:
        print("Choose a scraper")

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Run web scrapers. choose: scrapy or selenium spiders')
    parser.add_argument('--scrapy', dest='scrapy', default=False, action='store_true')
    parser.add_argument('--selenium', dest='selenium', default=False, action='store_true')
    parser.add_argument('--test', dest='test', default=False, action='store_true')
    args = parser.parse_args()
    main(args)