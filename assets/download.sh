wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz -O /tmp/geckodriver.tar.gz && \
tar -C /opt -xzf /tmp/geckodriver.tar.gz && chmod 755 /opt/geckodriver && \
ln -fs /opt/geckodriver /usr/bin/geckodriver && \
ln -fs /opt/geckodriver /usr/local/bin/geckodriver


# For mac use: brew install geckodriver