# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import os, sys
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'ks_protokoll')[0]
sys.path.append(parent_folder_path)

from database.src.client.sql_client import Database
import database.src.client.sql_scripts as scripts

class KsProtokollPipeline:
    def process_item(self, item, spider):
        return item


class SavePdfLinksPipeline(object):
    def __init__(self):
        self.session = Database.get_session()

    def process_item(self,item, spider):
        if type(spider).__name__ == "PdfSpider" or type(spider).__name__ == "DiariumSpider":
            pdf_links = item.get('pdfs')
            main_url = item.get('link')
            municipality_id = item.get('municipality_id')
            successful_inserts = 0
            for text, link in pdf_links:
                con = self.session.connect()
                insert_status = self.insert_file_info_url(con,main_url,text, link,municipality_id) #Note: insert link.text in the database
                if insert_status == True:
                    successful_inserts+=1
            self.update_municipalityinfo(n_pdfs=successful_inserts,municipality_id=municipality_id, urls_to_scrape=set(item.get('urls_to_scrape')))

    
    def update_municipalityinfo(self,n_pdfs,municipality_id,urls_to_scrape:dict):
        # scrape_status: 'complete', 'error', 'check_this_manually', 'todo'
        get_municipality_info = """
            SELECT %s 
            FROM ks_protokoll_schema.municipality_info
            WHERE 
                municipality_id=%s;
        """
        query = """
            set timezone to 'Europe/Paris';
            select now();
            UPDATE
                ks_protokoll_schema.municipality_info 
            SET 
                latest_update=NOW(),scrape_status=%s,n_pdfs=%s,urls_to_scrape=%s
            WHERE
                municipality_id=%s
        """
        con = self.session.connect()
        # print(self.session.fetch_all(con,get_municipality_info % ('n_pdfs',municipality_id)))
        con = self.session.connect()
        munspality_info_data = self.session.fetch_all(con,get_municipality_info % ('n_pdfs,urls_to_scrape',municipality_id))
        n_pdfs += munspality_info_data[0][0]

        urls_from_db = munspality_info_data[0][1]
        if urls_from_db is not None and type(urls_from_db)==list:
            print("#############################################################")
            print("######################  urls list from db ######################")
            print("#############################################################")
            print(urls_from_db)
            urls_to_scrape.update(munspality_info_data[0][1])

        if len(urls_to_scrape)>0:
            scrape_status = "in_progress"
        else:
            if n_pdfs<20 and n_pdfs >0:
                scrape_status = 'check_this_manually'
            elif n_pdfs==0:
                scrape_status = 'error'
            elif n_pdfs >250:
                scrape_status = 'check_this_manually'
            else:
                scrape_status = 'complete'
        con = self.session.connect()
        self.session.insert_one(con,query,(scrape_status,n_pdfs,list(urls_to_scrape),municipality_id))

    def insert_file_info_url(self,con,webpage_url,url_text,pdf_url,municipality_id):

        query =  """
            set timezone to 'Europe/Paris';
            select now();
            INSERT INTO ks_protokoll_schema.file_info
            (created_on,webpage_url,url_text,pdf_url,municipality_id)
            VALUES(NOW(),%s,%s,%s,%s);
        """
        con = self.session.connect()
        return self.session.insert_one(con,query,(webpage_url,url_text,pdf_url,municipality_id))


# saver = SavePdfLinksPipeline()

# saver.update_municipalityinfo(0,1)
