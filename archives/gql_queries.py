clear_all = """
mutation {
  delete_ks_protokoll_schema_file_info(where: {file_id: {_gt: 0}}) {
    affected_rows
  }
  delete_ks_protokoll_schema_municipality_info(where: {municipality_id: {_gt: 0}}) {
    affected_rows
  }
}
"""


insert_mun = """
mutation {
  insert_ks_protokoll_schema_municipality_info_one(object: {%s}) {
    created_on
    code
    name
    population
    main_group_code
    main_group_name
    municipality_group_code
    municipality_group_name
    county_code
    county_name
    region_name
    comment
    url
  }
}
"""

query = """
mutation {
    update_ks_protokoll_schema_municipality_info_by_pk(_set: {scrape_status: %s, n_pdfs: %d}, pk_columns: {municipality_id: %d}) {
        latest_update
        n_pdfs
        scrape_status
    }
}
"""
TRUNCATE TABLE ks_protokoll_schema.file_info RESTART IDENTITY;