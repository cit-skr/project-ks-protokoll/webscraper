from gql import Client, AIOHTTPTransport, transport
import gql
# Select your transport with a defined url endpoint
transport = AIOHTTPTransport(
    url="http://172.25.113.217:8080/v1/graphql",
    headers={'Content-Type': 'application/json', 'x-hasura-admin-secret': 'cloud_runner'}
)

# Create a GraphQL client using the defined transport
client = Client(transport=transport, fetch_schema_from_transport=True)

# # Provide a GraphQL query
# query = gql(
#     """
#     query GetData {
#         ks_protokoll_schema_file_info(limit: 10) {
#             municipality_id
#             last_usage
#             url_text
#         }
#     }
#     """
# )
def extract_insert_excel():
    import gql_queries

    import pandas as pd
    excel_file_path = "database/assets/KS_protokoll_url_20200519.xlsx"
    ks_protokoll_df = pd.read_excel(excel_file_path)
    for i in range(len(ks_protokoll_df)):
        row = ks_protokoll_df.iloc[i,:]
        comment = row['Comment'] if type(row['Comment']) is str else ''
        insert_items = {
            "code": int(row['Kommunkod']), 
            "name": str(row['Kommun']).strip(), 
            "population": int(row['Folkmängd 31 december 2019']), 
            "main_group_code": row['Huvudgrupp kod'],
            "main_group_name": row['Huvudgrupp namn'], 
            "municipality_group_code": int(row['Kommungrupp kod']),
            "municipality_group_name": row['Kommungrupp 2017 namn'], 
            "county_code": int(row['Länskod']), 
            "county_name": row['Län namn'], 
            "region_name": row['Landsting/region namn'], 
            "comment": comment.replace("'",''), 
            "url": row['Url']
        }
        

        items = (', '.join("{!s}: {!r} ".format(key,val) for (key,val) in insert_items.items())).replace('"','').replace("'",'"')
        q = gql_queries.insert_mun % str(items)

        try:
            r = client.execute(gql.gql(q))
        except BaseException as e:
            if 'unique' not in str(e):
                print(e)
                print(items)
  

query = """
mutation {
  update_ks_protokoll_schema_municipality_info_by_pk(pk_columns: {municipality_id: 10}, _set: {municipality_id: 11}) {
    municipality_id
  }
}

"""
scrape_status = "todo"
q = query # % (scrape_status,0,1)
print(q)
for i in range(3):
    try:
        r = client.execute(gql.gql(q))
    except gql.transport.exceptions.TransportQueryError as e:
        print("!!!!!!!")
        print(e.errors[0]['message'])
    except BaseException as e:
        print(e)


# print(r)