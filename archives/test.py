# import re
# # import textract
# from itertools import chain
# from urllib.parse import urlparse

# import scrapy
# from scrapy.linkextractors import LinkExtractor
# from scrapy.spiders import CrawlSpider, Rule
# from scrapy.crawler import CrawlerProcess

# import os, sys, math

# parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'ks_protokoll')[0]
# sys.path.append(parent_folder_path)

# from database.src.client.sql_client import Database
# import database.src.client.sql_scripts as scripts

# def parse_urls():
#     allowed_domains = []
#     urls_to_scrape = []
#     session = Database.get_session()
#     con = session.connect()
#     for row in session.get_municipality_info(con):
#         if True: #row.get('comment').strip() == "":
#             main_url = row.get('url')
#             parsed_url = urlparse(main_url)
#             allowed_domains.append(parsed_url.netloc[4:])
#             # urls_to_scrape.append((main_url,row.get('municipality_id')))
#             urls_to_scrape.append(main_url)

#     return allowed_domains, urls_to_scrape[0:2]


# class PdfSpider(CrawlSpider):
#     name = "hgjff"

#     allowed_domains, urls_to_scrape = parse_urls()
#     rules = [
#         Rule(
#             LinkExtractor(
#                 canonicalize=True,
#                 unique=True
#                 ), 
#             follow=True, callback="parse_item"
#             )
#         ]

#     def start_requests(self):
#         for i,(url,m_id) in enumerate(self.urls_to_scrape):
#             yield scrapy.Request(url=url.strip(),callback=self.parse, priority=i+1, meta={"id":m_id})

#     def parse(self, response):
#         extractor = LinkExtractor(allow_domains="www.website.com")
#         links = extractor.extract_links(response)
#         for link in links:
#                 yield scrapy.Request(link.url, callback=self.my_callback,) 

#     def parse_item(self, response):
#         atags = response.css('a[href$=".pdf"]::attr(href)')
#         pdf_links = [response.urljoin(atag.get()) for atag in atags]
#         # if response.meta.get('id') == None:
#         #     print("####################################################")
#         #     print(response)
#         #     sys.exit(1)
#         yield {'link': str(response.url), 'pdfs': pdf_links,"municipality_id":response.meta.get('id') }








#     # def parse(self,response):
#     #     self.logger.info('hello this is my first spider')
#     #     atags = response.css('a[href$=".pdf"]::attr(href)')
#     #     for atag in atags:
#     #         yield {'link': response.urljoin(atag.get())}

# # a,b = parse_urls()

# # print(len(a),len(b))

# import os, sys, math, time
# from urllib.parse import urlparse

# import scrapy
# from scrapy.linkextractors import LinkExtractor
# from scrapy.spiders import Rule, Spider
# from scrapy.crawler import CrawlerProcess
# from scrapy.selector import Selector
# from scrapy.http import Request

# import selenium
# from selenium.webdriver import Firefox, Chrome
# from selenium.webdriver.firefox.options import Options
# from selenium.webdriver.support.select import Select
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support import expected_conditions as EC

# parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'ks_protokoll')[0]
# sys.path.append(parent_folder_path)

# import ks_protokoll.spiders.utilities as utils

# class DiariumSpider(Spider):
#     name = "test_spider"
#     allowed_domains, urls_to_scrape = utils.parse_urls(filters="diarium,formpipe")

#     def start_requests(self):
#         for i,(url,m_id) in enumerate(self.urls_to_scrape):
#             yield scrapy.Request(url=url.strip(),callback=self.parse, priority=i+1, meta={"id":m_id, "p":i+1})
    
#     def parse(self, response):
#         try: 
#             print(response.url)
#             url = response.url
#             driver = utils.get_driver(url)
            
#             pdf_links = []
#             diarium_keywords = ["diari", "beslut-och"]
#             if any(x in str(url).lower() for x in diarium_keywords):
#                 pdf_links = utils.diarium_scraper(driver)
#             else:
#                 pdf_links = utils.formpipe_scraper(driver)
            
#             driver.quit()

#             yield {'link': str(url), 'pdfs': pdf_links,"municipality_id":response.meta.get('id') }

            
#         except BaseException as e:
#             print(e)
#             driver.quit()
#             yield {'link': str(response.url), 'pdfs': [],"municipality_id":response.meta.get('id') }


# # a, b = utils.parse_urls(filters="diarium, formpipe, check_this_manually")
# # print(a)
# # print(b)
# # print(len(b))
