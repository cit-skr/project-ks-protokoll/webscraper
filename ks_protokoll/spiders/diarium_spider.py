import os, sys, math, time
from urllib.parse import urlparse, urljoin

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Rule, Spider
from scrapy.crawler import CrawlerProcess
from scrapy.selector import Selector
from scrapy.http import Request

import selenium
from selenium.webdriver import Firefox, Chrome
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'ks_protokoll')[0]
sys.path.append(parent_folder_path)

import ks_protokoll.spiders.utilities as utils

class DiariumSpider(Spider):
    name = "diarium_spider"
    handle_httpstatus_list = [404,403,410]  

    def __init__(self, *args, **kwargs): 
        super(DiariumSpider, self).__init__(*args, **kwargs) 
        try:
            self.start_urls = [kwargs.get('start_url')] 
            self.domain_id = int(kwargs.get('domain_id'))
            self.is_test = int(kwargs.get('test'))
            fieldnames=['n_pdfs','municipality_id','http_status_code', 'url','error',"pdf_links"]
            self.csv_writer = utils.get_csv_writer(filename=f"output/diarium_spider_output.csv",fieldnames=fieldnames)
        except BaseException as e:
            print(e)

    def parse(self, response):
 
        url = str(response.url)
        pdf_links = []
        selenium_error = ""
        if int(response.status) < 300:

            ## Exctract pdfs via scrapy
            text = response.css('a::text')
            href = response.css('a::attr(href)')
            title = response.css('a::attr(title)')
            atags = (text,href,title)
            pdf_links += utils.pdf_filter(text,href,title)

            ## Exctract pdfs via selenium
            headless_driver = True
            if self.is_test: headless_driver = False
            driver = utils.get_driver(url,headless=headless_driver)
            try:  
                diarium_keywords = ["diari", "beslut-och"]
                if any(x in str(url).lower() for x in diarium_keywords):
                    pdf_links += utils.diarium_scraper(driver)
                else:
                    pdf_links += utils.formpipe_scraper(driver)
                time.sleep(5)
                driver.quit()
            except BaseException as e:
                selenium_error += str(e)
                driver.quit()
                
        domain = utils.get_domain(url)
        cleaned_pdf_links = {(text, urljoin(domain, url)) for text,url in pdf_links} # Note: Check how urls are joined
        info = {'municipality_id': self.domain_id,'http_status_code': response.status, 'n_pdfs': len(cleaned_pdf_links), 'url': str(url), 'error': selenium_error}

        self.csv_writer.writerow(info)
        with open(f"output/pdf_links.txt",'a+') as pf:
            pf.write("########################## --------------")
            pf.write(str(info)+"\n")
            for l in cleaned_pdf_links:
                pf.write(str(l[1])+"\n")

        if not self.is_test:
            info['pdf_links'] = cleaned_pdf_links
            yield info

