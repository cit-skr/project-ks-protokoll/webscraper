import re, csv, traceback
# import textract
from itertools import chain
from urllib.parse import urlparse

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule, Spider
from scrapy.crawler import CrawlerProcess


import os, sys, math

parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'ks_protokoll')[0]
sys.path.append(parent_folder_path)

from ks_protokoll.spiders.utilities import pdf_filter, get_csv_writer, get_domain, filter_and_clean_url, mkdir_p

class PdfSpider(Spider):
    name = "pdf_crawler"
    handle_httpstatus_list = [400,404,403,410]  

    def __init__(self, *args, **kwargs): 
        super(PdfSpider, self).__init__(*args, **kwargs) 
        try:
            self.start_urls = [kwargs.get('start_url')] 
            self.domain_id = int(kwargs.get('domain_id'))
            self.is_test = int(kwargs.get('test'))
            fieldnames=['n_pdfs','municipality_id','http_status_code', 'url','error',"pdf_links"]
            mkdir_p("output")
            self.csv_writer = get_csv_writer(filename=f"output/pdf_crawler_output.csv",fieldnames=fieldnames)
        except BaseException as e:
            print(e)
            raise

    def parse(self, response):
        url = str(response.url)
        pdf_links = []
        scrapy_error = ""
        if int(response.status) < 300:
            try:
                domain = get_domain(url)

                ## Extract links
                extractor = LinkExtractor(unique=True, allow=domain)
                
                pdfs_found = []; urls_to_crawl = []; pdfs_found_text = []

                links = extractor.extract_links(response)
                for link in links:
                    matches = ["protokoll", "ks", "kommunstyrelsen protokoll", "kommunstyrelse", "samman", "kommun","justerat",'mote'] # 'kommun',# Note: can be loaded from an external file
                    link_url = str(link.url); link_text = str(link.text)
                    if (link_url.startswith('http://') or link_url.startswith('https://')):
                        split_url = ' '.join(str(link_url).split('//')[1].split('/')[1:])
                        search_text = split_url.lower()+'_'+link_text.lower()
                        # print(link)
                        if any(x in search_text for x in ["pdf","download","content"]):
                            # print(link)
                            pdfs_found.append(link.url)
                            pdfs_found_text.append(str(link.text))
                        if any(x in search_text for x in matches):
                            if any(x in search_text for x in ["pdf","download","content"]):
                                pdfs_found.append(link.url)
                                pdfs_found_text.append(str(link.text))
                            else:
                                urls_to_crawl.append(link.url)
                ## Exctract pdfs
                text = response.css('a::text')
                init_hrefs = response.css('a::attr(href)')
                href = [filter_and_clean_url(response.urljoin(x.get())) for x in init_hrefs]
                title = response.css('a::attr(title)')
                atags = (text,href,title)
                pdf_links += pdf_filter(text,href,title)
                pdf_links += pdf_filter(pdfs_found_text,pdfs_found,[])
                pdf_links += pdf_filter(response.css('a[href$=".pdf"]::text'),response.css('a[href$=".pdf"]::attr(href)'),[])
            
                ## follow links with a max limit of 200
                for url in urls_to_crawl: #[:200]: # Note: Maybe change the max crawl limit?
                    yield scrapy.Request(url, callback=self.parse)
    
               
                
            except BaseException as e:
                scrapy_error += str(e)
                print(e)
                traceback.print_exc()


        # cleaned_pdf_links = {(text, response.urljoin(_url)) for text,_url in pdf_links} # Note: Check how urls are joined
        info = {'municipality_id': self.domain_id,'http_status_code': response.status, 'n_pdfs': len(pdf_links), 'url': url, 'error': scrapy_error}
        self.csv_writer.writerow(info)
        with open(f"output/pdf_links_pdf_crawler.txt",'a+') as pf:
            for l in pdf_links:
                pf.write(str(l)+"\n")
        if not self.is_test:
            info['pdf_links'] = pdf_links
            yield info
        
