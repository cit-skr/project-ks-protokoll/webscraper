import sys, os, time, re, csv, requests, traceback, errno
from urllib.parse import urlparse
from collections import OrderedDict

import selenium
from selenium.webdriver import Firefox, Chrome
from selenium.webdriver.firefox.options import Options as ff_options
from selenium.webdriver.chrome.options import Options as c_options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import scrapy
from scrapy import Spider
from scrapy.selector import Selector
from scrapy.http import Request

parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'ks_protokoll')[0]
sys.path.append(parent_folder_path)


def mkdir_p(path, folder_name = None):
    if folder_name:
        abs_folder_path = os.path.join(path, folder_name)
    else:
        abs_folder_path = path
    try:
        os.makedirs(abs_folder_path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(abs_folder_path):
            pass
        else:
            traceback.print_exc()

import re
# find_year = re.compile(r'.*(20[0-9]{2}?)')
# find_month = re.compile(r'.*(jan(?:uari)?|feb(?:ruari)?|mar(?:s)?|apr(?:il)?|may|jun(?:i)?|jul(?:i)?|aug(?:usti)?|sep(?:tember)?|okt(?:ober)?|(nov|dec)(?:ember)?)')
# # find_date = re.compile(r'.*([- /]?[0-3][0-9][- /]?(?:[0-9]{2})?[0-9]{2})')
# find_date = re.compile(r'.*([- /]?(0[1-9]|1[0-2])[- /](0[1-9]|[12]\d|3[01]))')
# find_md = re.compile(r'.*([- /]?(0[1-9]|1[0-2])[- /]([0-2][0-9]|3[0-1])')
# find_full_date = re.compile(r'.*(20[0-9]{2}?[- /]?[0-3]?[0-9][- /]?(?:[0-9]{2})?[0-9]{2})')


find_year = re.compile(r'.*(20[1-2][0-9])')
find_month = re.compile(r'.*(jan(?:uari)?|feb(?:ruari)?|mar(?:s)?|apr(?:il)?|may|jun(?:i)?|jul(?:i)?|aug(?:usti)?|sep(?:tember)?|okt(?:ober)?|(nov|dec)(?:ember)?)')
find_full_date = re.compile(r'.*(20[0-9]{2}?[- /]?[0-3]?[0-9][- /]?(?:[0-9]{2})?[0-9]{2})')
find_md = re.compile(r'.*((20[1-2][0-9])[- /]?(0[1-9]|1[0-2])[- /]?([0-2][0-9]|3[0-1]))')

def get_date(string):
    y = find_year.findall(string) 
    m = find_month.findall(string)
    if len(m)>0:
        m = [m[0][0]]
    ymd = find_full_date.findall(string)
    md = find_md.findall(string)
    date_extract = {'y':y, 'm':m, 'md':md, 'ymd':ymd }
    return date_extract

def get_csv_writer(filename="website_info.csv",fieldnames = ['pdfs','links','info','domain','url']):
    append_mode = False
    try:
        csv_file = open(filename,mode='a+')
        append_mode = True
    except:
        csv_file = open(filename,mode='w')
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    if not append_mode: writer.writeheader()
    return writer

def filter_and_clean_url(url):
    cleaned_url = None
    try:
        if (url.startswith('http://') or url.startswith('https://')):
            split_url = str(url).split('//')
            resources = OrderedDict.fromkeys(split_url[1].split('/')[1:])
        
            if len(resources) >=2:
                # print(url)
                joined_resources = ' '.join(list(resources)[-3:])
                end_part = list(resources)[-1]
                dates = find_full_date.findall(joined_resources)
                year = find_year.findall(joined_resources)
                month = find_month.findall(joined_resources)
                md = find_md.findall(joined_resources)

                if (len(md)>=1 or len(month)>=1 or len(year)>=1) or ('pdf' in end_part):
                    cleaned_url = split_url[0]+ '//' + '/'.join(OrderedDict.fromkeys(split_url[1].split('/')))
                
                # print(cleaned_url)
    except BaseException as e:
        pass
    return cleaned_url


def pdf_filter(texts, hrefs, titles):
    pdf_links = []
    keywords = ['kommunstyrelse', 'protokoll', 'sammanträde', 'ks','justerat','revision','mote'] # Note: do some extra filtering on the links here!!
    keywords_extra = ['kommunstyrelse', 'protokoll', 'sammanträde', 'ks'] # 'protokoll',  '-ks-', 'ks','kommun', 'utskott', 'rev', 'revision','mote',
    neg_keywords = ['kallelse','sutdrag','byggnad', 'arbetsuts', 'barn','omvardnad','html' ]
    texts_exist = len(texts)==len(hrefs)
    titles_exist = len(titles)==len(hrefs)
    try:
        for i in range(len(hrefs)):
            if type(hrefs[i]) is str or hrefs[i] is None:
                href = hrefs[i]
            else:
                href = hrefs[i].get()
            href = filter_and_clean_url(href)
            if href is not None:
                hl = ' '.join(href.split('//')[1].split('/')[-2:]).lower()

                text  = ""
                if texts_exist:
                    if type(texts[i]) is str:
                        text += texts[i].lower()
                    else:
                        text += texts[i].get().lower()
                if titles_exist:    
                    text += "--"+titles[i].get().lower()

                date_href = get_date(href)
                date_text = get_date(text)
                date_href_vals = sum(date_href.values(),[])
                date_text_vals = sum(date_text.values(),[])
                date_check = 0
                if len(date_href_vals)>0:
                    date_check+=1
                    text += '--'+date_href.__str__()
                if len(date_text_vals)>0:
                    date_check+=1
                    text += '--'+date_text.__str__()
                keyword_check = any(x in hl for x in keywords) and not any(x in hl+text for x in neg_keywords) 
                if keyword_check : #and any(x in text for x in keywords_extra):
                    pdf_links.append((text ,href))
        return pdf_links
    except BaseException as e:
        print("########################################------- ", e)
        traceback.print_exc()



def get_domain(url):
    slicer = ''; _slice = True
    if '.com' in str(url):
        slicer = '.com'
    elif '.se' in str(url):
        slicer = '.se'
    else:
        _slice = False
        
    if _slice: 
        domain = url.split(slicer+'/')[0]+ slicer
    else:
        domain = url
    return domain
    
def scrapy_link_extractor(driver):
    scrapy_selector = Selector(text = driver.page_source)
    text = scrapy_selector.css('a::text')
    hrefs = scrapy_selector.css('a::attr(href)')
    title = scrapy_selector.css('a::attr(title)')
    pdf_links = pdf_filter(text, hrefs, title)
    return pdf_links


def get_driver(url, headless=True):
    opts = ff_options()
    opts.headless = headless
    opts.set_preference("browser.helperApps.neverAsk.saveToDisk", 'application/pdf')
    driver = Firefox(options=opts)
    
    try:
        driver.get(url)
        time.sleep(6)
        return driver
    except BaseException as e:
        print(e)
        return driver

class ButtonClicker(object):
    def __init__(self,driver):
        self.buttons = driver.find_elements_by_xpath("//button")

    def click(self,button_name="Sök"):
        error = 1
        for button in self.buttons:
            if button.text == button_name:
                error = 0
                button.click()
                return error
        return error

    def loop_clicks(self,button_name="Sök",delay=2):
        n_errors = 0
        while True:
            n_errors += self.click(button_name=button_name)
            time.sleep(delay)
            if n_errors>2:
                break

def drop_down_picker(driver, title = '-- Välj informationstyp', option = "Möte"):
    try:
        drp_down = driver.find_element_by_xpath(f"//button[@title='{title}']")
        drp_down.send_keys(option)
        drp = driver.find_element_by_link_text(option)
        drp.click()
        # print(f"Succesfully clicked {title} with option: {option}")
        return 0
    except BaseException as e:
        print(e)
        return 1

def expand_table(driver):
    try:
        table = driver.find_element_by_xpath("//table")
        names = table.text.split('\n')
        for item in names:
            # print(item)
            td = table.find_element_by_xpath(f"//tr/td[contains(text(),'{item.split(' ')[1]}' )]")
            td.click()
            time.sleep(2)
            # atags = table.find_elements_by_css_selector("a")
            # print(len(atags))
            # time.sleep(2)
            td.click()
            # time.sleep(2)
        # print("Successfully expanded table!")
    except BaseException as e:
        print(e)


def enter_input_text(driver, field = "placeholder='Sök'", search_text = "kommunstyrelsen protokoll"):
    try:
        text_box = driver.find_element_by_xpath(f"//input[@{field}]")
        text_box.send_keys(search_text)
        text_box.send_keys(Keys.RETURN)
        
        # print(f"Succesfully clicked {field} with option: {search_text}")
    except BaseException as e:
        print(e)

def click_input(driver, field = "value='Visa fler resultat...'"):
    try:
        text_box = driver.find_element_by_xpath(f"//input[@{field}]")
        text_box.click()
        
        # print(f"Succesfully clicked {field} with option: {search_text}")
        return 0
    except BaseException as e:
        print(e)
        return 1


def atag_clicker(driver, field="@aria-label='Nästa sida'"):
    try:
        atag = driver.find_element_by_xpath(f"//a[@{field}]")
        atag.click()
        
        # print(f"Succesfully clicked {field}")
        return 0
    except BaseException as e:
        print(e)
        return 1

        
def diarium_form_filler(driver):
    error_count = 0
    error_count += drop_down_picker(driver, title='-- Välj informationstyp', option="Möte")
    error_count += drop_down_picker(driver, title='Hela diariet', option="Möte")

    error_count += drop_down_picker(driver, title='Alla nämnder', option="Kommunstyrelsen")
    error_count += drop_down_picker(driver, title='Alla nämnder', option="Kommunstyrelsen/Kommunfullmäktige")  
    error_count += drop_down_picker(driver, title='Alla beslutinstanser', option="Kommunstyrelsen")
    if error_count>4:
        error_count += drop_down_picker(driver, title='Alla', option="Kommunfullmäktige/Kommunstyrelsen")
        # Note: add date picker here
        error_count += drop_down_picker(driver, title='Dokument', option="Protokoll")

    # print(error_count)
    time.sleep(2)

    return error_count

def atag_click_loop(driver, field="title='Nästa'", max_clicks = 25):
    links = []
    n_clicks = 0
    n_errors = 0
    while True:
        n_errors += atag_clicker(driver,field=field)
        time.sleep(1)
        links += scrapy_link_extractor(driver)
        if n_errors>2 or n_clicks>=max_clicks:
            break
        n_clicks += 1
    # print("completed atag loop clicks!")
    return links

def diarium_scraper(driver):
    links = []
    button_clicker = ButtonClicker(driver)
    error_count = diarium_form_filler(driver)
    if error_count<=5:
        # print("simple diarium mode")
        time.sleep(2)
        button_clicker.click(button_name="Sök")
        time.sleep(3)
        button_clicker.loop_clicks(button_name="Visa fler",delay=2)
        time.sleep(3)
        expand_table(driver)
        links += scrapy_link_extractor(driver)
    else:
        enter_input_text(driver,field="placeholder='Ange sökord'",search_text='kommunstyrelsen protokoll')
        time.sleep(2)
        links = atag_click_loop(driver,field="title='Nästa'", max_clicks=30)
    return links
    

def formpipe_scraper(driver):
    links = []
    enter_input_text(driver,field="placeholder='Sök'",search_text='kommunstyrelsen protokoll')
    enter_input_text(driver,field="placeholder='Ange sökord'",search_text='kommunstyrelsen protokoll')
    time.sleep(3)
    links += atag_click_loop(driver,field="aria-label='Nästa sida'", max_clicks=2)
    links += atag_click_loop(driver,field="aria-label='Next'", max_clicks=2)
    links += atag_click_loop(driver,field="title='Nästa'", max_clicks=2)

    enter_input_text(driver,field="placeholder='Sök'",search_text='ks protokoll')
    enter_input_text(driver,field="placeholder='Ange sökord'",search_text='ks protokoll')
    time.sleep(3)
    links += atag_click_loop(driver,field="aria-label='Nästa sida'", max_clicks=2)
    links += atag_click_loop(driver,field="aria-label='Next'", max_clicks=2)
    links += atag_click_loop(driver,field="title='Nästa'", max_clicks=2)
    return links