# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import os, sys
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'ks_protokoll')[0]
sys.path.append(parent_folder_path)


from gql import gql, Client, AIOHTTPTransport, transport
import gql as gql_lib

# Select your transport with a defined url endpoint


class KsProtokollPipeline:
    def process_item(self, item, spider):
        return item


class SavePdfLinksPipeline(object):
    def __init__(self):
        transport = AIOHTTPTransport(
            url="http://172.25.113.217:8080/v1/graphql",
            headers={'Content-Type': 'application/json', 'x-hasura-admin-secret': 'cloud_runner'}
        )

        # Create a GraphQL client using the defined transport
        self.client = Client(transport=transport, fetch_schema_from_transport=True)

    def process_item(self,item, spider):
        if type(spider).__name__ == "PdfSpider" or type(spider).__name__ == "DiariumSpider":
            pdf_links = item.get('pdf_links')
            main_url = item.get('url')
            municipality_id = item.get('municipality_id')
            http_status_code = item.get('http_status_code')
            error = item.get('error')
            n_pdfs = item.get('n_pdfs')

            successful_inserts = 0
            insert_error = ""
            
            for text, link in pdf_links:
                try:
                    insert_status = self.insert_file_info_url(main_url,text, link,municipality_id) #Note: insert link.text in the database
                    if 'errors' not in insert_status:
                        successful_inserts+=1
                    else:
                        insert_error = str(insert_status['errors'][0]['message'])
                except gql_lib.transport.exceptions.TransportQueryError as e:
                    insert_error = str(e.errors[0]['message'])
                except BaseException as e:
                    print(e)

            status = {'scrape_status': '','successful_inserts':successful_inserts, 'error': error, 'insert_error': insert_error.replace('"','') }
            self.update_municipalityinfo(n_pdfs,municipality_id, http_status_code,status)

    
    def update_municipalityinfo(self,n_pdfs,municipality_id,http_status_code,status):
        # scrape_status: 'complete', 'error', 'check_this_manually', 'todo'
        get_municipality_info = """
            query MyQuery {
                ks_protokoll_schema_municipality_info_by_pk(municipality_id: %d) {
                    n_pdfs
                }
            }
        """
        query = """
            mutation {
                update_ks_protokoll_schema_municipality_info_by_pk(_set: {status: "%s", n_pdfs: %d, http_status_code: %d}, pk_columns: {municipality_id: %d}) {
                    latest_update
                    n_pdfs
                    status
                    http_status_code
                }
            }
        """
    
        munspality_info_data = self.client.execute(gql(get_municipality_info % (municipality_id)))
        n_pdfs += munspality_info_data.get('ks_protokoll_schema_municipality_info_by_pk').get('n_pdfs')


        if n_pdfs<10 and n_pdfs >0:
            scrape_status = "in_progress"
        elif n_pdfs==0:
            scrape_status = "todo"
        elif n_pdfs >250:
            scrape_status = "check_this_manually"
        else:
            scrape_status = "complete"

        status['scrape_status'] = scrape_status

        print(status)

        self.client.execute(gql(query % (str(status),n_pdfs, http_status_code,municipality_id)))

    def insert_file_info_url(self,webpage_url,url_text,pdf_url,municipality_id):

        query =  """
            mutation {
                insert_ks_protokoll_schema_file_info_one(object: {webpage_url: "%s", url_text: "%s", pdf_url: "%s", municipality_id: %d}) {
                    created_on
                    webpage_url
                    url_text
                    pdf_url
                    municipality_id
                }
            }
        """
        return self.client.execute(gql(query % (webpage_url,url_text,pdf_url,municipality_id)))



